package link.shapkin.gpd

import cats.effect.{ExitCode, IO, IOApp}

object GpdIOApp extends IOApp {
  def run(args: List[String]): IO[ExitCode] =
    GpdApp.appResource[IO](args).use(_.downloadProjects).as(ExitCode.Success)
}
