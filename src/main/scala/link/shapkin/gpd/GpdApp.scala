package link.shapkin.gpd

import cats.effect.{Concurrent, ContextShift, Resource, Sync}
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.option._
import cats.syntax.traverse._
import com.tylip.common.ModifyFor._
import org.gitlab4j.api.models.{PipelineFilter, PipelineStatus, ProjectFilter}
import org.gitlab4j.api.{Constants, GitLabApi}
import org.glassfish.jersey.client.ClientProperties
import org.typelevel.log4cats.MessageLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import java.io.{File, FileInputStream, FileOutputStream}
import java.net.URI
import java.net.http.HttpResponse.BodyHandlers
import java.net.http.{HttpClient, HttpRequest}
import java.util.Date
import java.util.zip.ZipInputStream
import scala.jdk.CollectionConverters._

final case class GpdConfig(
  maybeToken: Option[String] = None,
  dir: String = "/var/opt/gitlab-pubs-downloader",
)

object GpdApp {

  import scopt.OParser
  private val builder = OParser.builder[GpdConfig]
  private val parser1 = {
    import builder._
    OParser.sequence(
      programName("gitlab-pubs-downloader"),
      opt[String]("token").action((x, c) => c.copy(maybeToken = x.some)),
      opt[String]("dir").action((x, c) => c.copy(dir = x)),
    )
  }

  private def getLastModified(directory: File): Option[Date] =
    directory.listFiles
      .flatMap(file =>
        if (file.getName != ".stfolder")
          if (file.isFile) new Date(file.lastModified()).some
          else getLastModified(file)
        else None,
      )
      .sorted
      .lastOption

  def appF[F[_]: Concurrent: ContextShift](
    args: List[String],
  ): F[GdpApp[F]] =
    for {
      implicit0(logger: MessageLogger[F]) <- Slf4jLogger.create
      config <- Sync[F].fromEither(
        OParser
          .parse(parser1, args, GpdConfig())
          .toRight(new Exception("Wrong command line arguments!")),
      )
      token <- Sync[F].fromOption(config.maybeToken, new Exception("no token"))
      gitlabApi =
        new GitLabApi(
          "https://gitlab.com",
          token,
          null,
          Map[String, Object](
            ClientProperties.CONNECT_TIMEOUT -> Integer.valueOf(30000),
          ).asJava,
        )
      maybeAfter <- Sync[F].delay(
        new File(config.dir).some.filter(_.exists()).flatMap(getLastModified),
      )
      http <- Sync[F].delay(
        java.net.http.HttpClient
          .newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build(),
      )
    } yield new GdpApp[F](gitlabApi, http, maybeAfter, config.dir)

  def appResource[F[_]: Concurrent: ContextShift](
    args: List[String],
  ): Resource[F, GdpApp[F]] =
    for {
      app <- Resource.eval(appF(args))
    } yield app

}

final class GdpApp[F[_]: Concurrent: ContextShift: MessageLogger](
  gitLabApi: GitLabApi,
  http: HttpClient,
  after: Option[Date],
  dir: String,
) {

  private val pipelineFilter =
    (new PipelineFilter)
      .withStatus(PipelineStatus.SUCCESS)
      .withOrderBy(Constants.PipelineOrderBy.UPDATED_AT)
      .withSort(Constants.SortOrder.ASC)
      .modifyFor(after)(_.withUpdatedAfter(_))

  private val projectFilter =
    (new ProjectFilter)
      .withMembership(true)
      .withProgrammingLanguage("TeX")
      .withOrderBy(Constants.ProjectOrderBy.UPDATED_AT)
      .withArchived(false)
      .withSortOder(Constants.SortOrder.ASC)

  private def unzip(zipPath: String, unzipDir: String): F[Unit] =
    Sync[F].delay {
      val fis = new FileInputStream(zipPath)
      val zis = new ZipInputStream(fis)
      LazyList.continually(zis.getNextEntry).takeWhile(_ != null).foreach {
        file =>
          val path = s"$unzipDir/${file.getName}"
          new File(path).getParentFile.mkdirs
          val fout = new FileOutputStream(path)
          val buffer = new Array[Byte](1024)
          LazyList
            .continually(zis.read(buffer)).takeWhile(_ != -1).foreach(
              fout.write(buffer, 0, _),
            )
          fout.close()
          new File(path).setLastModified(file.getTime)
      }
      fis.close()
      zis.close()
    }

  private def processZip(unZipDir: String, zipPath: String, zipFile: File) =
    for {
      _       <- MessageLogger[F].info(s"downloaded: $zipFile")
      _       <- unzip(zipPath, unZipDir)
      deleted <- Sync[F].delay(zipFile.delete())
      _ <- Sync[F].fromEither(
        Either.cond(deleted, (), new Exception(s"Cannot delete $zipPath")),
      )
    } yield ()

  private def getArtifact(
    projectId: Long,
    branch: String,
    zipFile: File,
  ): F[Unit] = {
    val uri =
      new URI(
        s"https://gitlab.com/api/v4/projects/$projectId/jobs/artifacts/$branch/download?job=build",
      )
    Sync[F]
      .delay(
        http.send(
          HttpRequest
            .newBuilder()
            .uri(uri)
            .header("PRIVATE-TOKEN", gitLabApi.getAuthToken)
            .GET()
            .build(),
          BodyHandlers.ofFile(zipFile.toPath),
        ),
      )
      .flatMap(response =>
        if (response.statusCode() == 404)
          MessageLogger[F].warn(s"Status ${response.statusCode()} for $uri")
        else if (response.statusCode() != 200)
          Sync[F].raiseError[Unit](
            new Exception(s"Status ${response.statusCode()} for $uri"),
          )
        else if (
          !response
            .headers().allValues("content-type").contains(
              "application/octet-stream",
            )
        )
          Sync[F].raiseError[Unit](
            new Exception(
              s"type ${response.headers().allValues("content-type")} for $uri",
            ),
          )
        else Sync[F].unit,
      )
  }

  private def downloadBranch(
    projectId: Long,
    projectPath: String,
    branch: String,
  ): F[Unit] = {
    val unZipDir = s"$dir/$projectPath/$branch"
    val zipPath = s"$unZipDir.zip"
    val zipFile = new File(zipPath)
    for {
      _ <- MessageLogger[F].info(s"downloading: $projectPath branch $branch")
      _ <- Sync[F].delay(new File(unZipDir).mkdirs())
      _ <- Sync[F].delay(zipFile.createNewFile())
      _ <- getArtifact(projectId, branch, zipFile)
      _ <- processZip(unZipDir, zipPath, zipFile)
    } yield ()
  }

  private def downloadProject(projectId: Long, projectPath: String): F[Unit] =
    for {
      _ <- MessageLogger[F].info(s"getting pipelines for $projectPath")
      pipelines <- Sync[F].delay(
        gitLabApi.getPipelineApi.getPipelines(projectPath, pipelineFilter),
      )
      _ <- pipelines.asScala.toList
        .map(_.getRef).distinct.traverse(
          downloadBranch(projectId, projectPath, _),
        )
    } yield ()

  def downloadProjects: F[Unit] =
    for {
      projectsJ <-
        Sync[F].delay(gitLabApi.getProjectApi.getProjects(projectFilter))
      projects = projectsJ.asScala.toList
      _ <- MessageLogger[F].info(s"processing ${projects.length} projects")
      _ <- projects.traverse(project =>
        downloadProject(project.getId, project.getPathWithNamespace),
      )
    } yield ()

}
