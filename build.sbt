scalaVersion := "2.13.10"
name := "gitlab-pubs-downloader"
organization := "link.shapkin"
version := "0.1.0-SNAPSHOT"

// renovate: datasource=docker depName=eclipse-temurin extractVersion=^(?<version>[\d_\.]+)-jre-alpine$ versioning=loose
val javaVersion = "17.0.5_8"

val tylipPublic =
  "tylip-public" at "https://nexus.tylip.com/repository/tylip-public/"

resolvers += tylipPublic

libraryDependencies ++= Vector(
  "org.gitlab4j" % "gitlab4j-api" % "5.7.0",
  "com.github.scopt" %% "scopt" % "4.1.0",
  "org.typelevel" %% "cats-core" % "2.12.0",
  // cats-effect 3 not supported yet
  "org.typelevel" %% "cats-effect" % "2.5.3", // scala-steward:off
  "org.typelevel" %% "log4cats-slf4j" % "1.7.0",
  "ch.qos.logback" % "logback-classic" % "1.5.11",
  "com.tylip" %% "tylip-common-core" % "0.1.1",
  "org.apache.commons" % "commons-lang3" % "3.17.0",
)

scalafmtOnCompile := !insideCI.value

scalacOptions ++= Seq("-deprecation")

Compile / javacOptions ++= Seq("-encoding", "UTF-8")

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")

enablePlugins(AshScriptPlugin, DockerPlugin)

dockerBaseImage := s"eclipse-temurin:$javaVersion-jre-alpine"

dockerExposedVolumes += "/var/opt/gitlab-pubs-downloader"
