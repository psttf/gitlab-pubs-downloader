Gitlab Pubs Downloader
======================

Auto-download GitLab artifacts (especially publications such as LaTeX repos) available for the user. Especially useful in conjunction with [Syncthing](https://syncthing.net/) to distribute downloaded PDF files to linked devices such as tablets etc.

Usage
-----

Example `crontab`:

```
* * * * * sudo docker run --rm --network host -u 1003 -v /home/ps-gitlab-pubs/data:/var/opt/gitlab-pubs-downloader registry.gitlab.com/psttf/gitlab-pubs-downloader/gitlab-pubs-downloader:latest --token <USER_GITLAB_TOKEN>
```
